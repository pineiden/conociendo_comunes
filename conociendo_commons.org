#+TITLE: Conociendo los Comunes (Commons)
#+AUTHOR: David Pineda Osorio

* Contextualización
* Conociendo el Concepto
* Los principios de un sistema de comunes
* Alcances y desafíos
* Formas de vidas que se posibilitan
* Nuevas prácticas económicas
* Nuestros límites, el conocimiento holístico
* Un mundo conectado
** Una red
** Redes de comunidades
** Una infraestructura vitalista
* Herramientas para el cambio social
* Construyendo un mundo distinto y mejor
* Autores y textos destacados
